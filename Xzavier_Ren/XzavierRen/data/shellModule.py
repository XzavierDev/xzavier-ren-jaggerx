#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# FIRST: Import Dependencies
# SECOND: Set Application Paths
# THIRD: Scan application paths, append standalone modules to standaloneapps, directory based apps to dirapps
#(do not distinguish between system/normal)

#FOURTH: If app ends with SYS, then sysapp. Strip SYS from command input.

#1:
AppName = "shellModule"
MinKern = [0,9,2,0]
MinPyth = [3,5,0]
AppVersion = [0,9,2,0]
AppType = "CORE"
Framework1 = False
PlatformID = ["ALL"]
EditionID = ["_foundation"]
InstallType = "OAM"
DevID = 0000
#AppCode

#########################################################################
# COMMENTS SECTION:                                                     #
# -----------------                                                     #
#                                                                       #
# - This module obsoletes the registry. The final version of this build #
# should have the registry removed.                                     #
#                                                                       #
#########################################################################

#1
#jaggerx
import transientStorageModule as alwaysonrd
import os
import sys
import linecache
import importlib
import time
import platform
import os_lib as od

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')
print (workingdir)
isnt = linecache.getline("dirinfo3.cfg", 2).rstrip('\n')

prompt = "JAGGERX> "

#2
appdir = ""+workingdir+"/apps/apps"
print (appdir)
appcommdir = ""+workingdir+"/apps/appcomm"
sysdir = ""+workingdir+"/apps/system"
syscommdir = ""+workingdir+"/apps/system/syscomm"
switch = False

sys.path.append(appdir)
sys.path.append(sysdir)

importedmodules = {}
launch = "null"
dummy = "init"
dummyapp = dummy+""+launch+""
dashdummy = "dashboardModule"

wasnothing = False
importedmodules[dashdummy] = importlib.import_module(dashdummy)
app = ""


while True:
	alwaysonrd.ramslot3 = "NONE"
	alwaysonrd.ramslot3 = ""
	print(alwaysonrd.ramslot3)
	if wasnothing == True:
		wasnothing = False
	else:
		os.system(""+od.cl+"")
		importlib.reload(importedmodules[dashdummy])
	launch = input(""+prompt+" ")

	if launch == "modoption" and launch not in importedmodules:
		specapp = "modLoaderModule"
		importedmodules[launch] = importlib.import_module(specapp)
		importedmodules[launch].modSelector()
	elif launch == "modoption" and launch in importedmodules:
		specapp = "modLoaderModule"
		importlib.reload(importedmodules[specapp])
		importedmodules[launch].modSelector()
	else:
		pass
	app = launch

	if "/" in launch:
		assertcommand = launch.split("/")
		command = assertcommand[0].rstrip()
	else:
		command = launch
		pass
	lencommand = len(launch)
	charcount = 0
	for char in launch:
		while charcount <= lencommand-1:
			char = launch[charcount]
			if char == "/":
				switch = True
				charfreeze = charcount
				break
			else:
				charcount = charcount+1
				continue
		if switch == True:
			charcount = charfreeze+1
			switchcommand = []
			for char in launch:
				while charcount <= lencommand-1:
					switchcommand.append(launch[charcount])
					charcount = charcount+1
					switchproper = "".join(switchcommand)
					alwaysonrd.ramslot3 = (""+switchproper+"")
		else:
			alwaysonrd.ramslot3 = ""


	if launch == "":
		wasnothing = True
	else:
		pass
	dummyapp = dummy+""+launch+""
	if os.path.exists(""+appdir+"/"+launch+".py") or os.path.exists(""+appdir+"/"+launch+"") and (launch) not in importedmodules:
		if os.path.isdir(""+workingdir+"/apps/"+launch+".py") == True:
			path = ""+workingdir+"/apps/"+launch+""
			os.chdir(""+path+"")
			importedmodules[launch] = importlib.import_module(dummyapp)

		elif os.path.isdir(""+appdir+"/"+launch+"") == True and launch != "":
			path = ""+appdir+"/"+launch+""
			os.chdir(""+path+"")
			sys.path.append(path)
			importedmodules[launch] = importlib.import_module(dummyapp)
			time.sleep(3)
			os.system('clear')
			importlib.reload(importedmodules[dashdummy])

		elif launch == "":
			pass
		else:
			path = ""+appdir+""
			os.chdir(""+path+"")
			importedmodules[launch] = importlib.import_module(launch)
			if launch not in importedmodules:
				importlib.reload(importedmodules[launch])
				time.sleep(3)
				os.system('clear')
				importlib.reload(importedmodules[dashdummy])
			else:
				pass

	elif os.path.exists(""+appdir+"/"+launch+".py") or os.path.exists(""+appdir+"/"+launch+"") and launch in importedmodules:
		if os.path.isdir(""+workingdir+"/apps/"+launch+"") == True:
			path = ""+workingdir+"/apps/"+launch+""
			os.chdir(""+path+"")
			importlib.reload(importedmodules[dummyapp])
			alwaysonrd.ramslot3 = ""
		else:
			importlib.reload(importedmodules[launch])
			alwaysonrd.ramslot3 = ""

	elif os.path.exists(""+sysdir+"/"+launch+".py") or os.path.exists(""+sysdir+"/"+launch+"") and (launch) not in importedmodules:
		if os.path.exists(""+workingdir+"/apps/system/"+launch+".py") == True:
			path = ""+workingdir+"/apps/"+launch+""
			os.chdir(""+path+"")
			dirindicate = open("dirinfo4.cfg","w")
			dirindicate.write(""+workingdir+"")
			dirindicate.close()
			importedmodules[launch] = importlib.import_module(dummyapp)
			alwaysonrd.ramslot3 = ""

		elif os.path.exists(""+sysdir+"/"+launch+"") == True and launch != "":
			alwaysonrd.ramslot3 = ""
			path = ""+sysdir+"/"+launch+""
			os.chdir(""+path+"")
			dirindicate = open("dirinfo4.cfg","w")
			dirindicate.write(""+workingdir+"")
			dirindicate.close()
			sys.path.append(path)
			importedmodules[launch] = importlib.import_module(dummyapp)
			alwaysonrd.ramslot3 = ""

		elif launch == "":
			alwaysonrd.ramslot3 = ""
		else:
			path = ""+sysdir+""
			print (path)
			os.chdir(""+path+"")
			importedmodules[launch] = importlib.import_module(launch)
			if launch not in importedmodules:
				importlib.reload(importedmodules[launch])
				alwaysonrd.ramslot3 = ""
			else:
				alwaysonrd.ramslot3 = ""

	elif os.path.exists(""+sysdir+"/"+launch+".py") or os.path.exists(""+sysdir+"/"+launch+"") and launch in importedmodules:
		if os.path.isdir(""+workingdir+"/apps/"+launch+"") == True:
			path = ""+workingdir+"/apps/system/"+launch+""
			os.chdir(""+path+"")
			dirindicate = open("dirinfo4.cfg","w")
			dirindicate.write(""+workingdir+"")
			dirindicate.close()
			importlib.reload(importedmodules[dummyapp])
			alwaysonrd.ramslot3 = ""
		else:
			importlib.reload(importedmodules[launch])
			alwaysonrd.ramslot3 = ""

	elif launch == "":
		alwaysonrd.ramslot3 = ""

	elif launch == "winreset" or platform.system() == "Windows" and launch == "recovery":
		import winResetModule

	elif (command) == "recovery" and platform.system() != "Windows":
	   print("Setting next boot to recovery.")
	   if isnt != "1":
		   home = os.environ['HOME']
	   else:
		   home = os.environ['HOMEPATH']
		   home = home+"\\Documents"
		   home = "C:"+(home)
		   print (home)
	   if isnt == "1":
	       writerecoverybit = open(""+home+"\\recoveryinfo.cfg","w")
	       writerecoverybit.write("1")
	       writerecoverybit.close()
	   else:
		   writerecoverybit = open(""+home+"/Documents/recoveryinfo.cfg","w")
		   writerecoverybit.write("1")
		   writerecoverybit.close()
	   exit()

	elif "/" in launch:
		speclaunch = launch.split("/")
		speclaunch = speclaunch[0].rstrip()
		if os.path.isdir(""+sysdir+"/"+speclaunch+"") == True and launch != "" and speclaunch not in importedmodules:
			path = ""+sysdir+"/"+speclaunch+""
			os.chdir(""+path+"")
			os.chdir(""+path+"")
			dirindicate = open("dirinfo4.cfg","w")
			dirindicate.write(""+workingdir+"")
			dirindicate.close()
			sys.path.append(path)
			dummyapp = "init"+speclaunch
			importedmodules[speclaunch] = importlib.import_module(dummyapp)
			time.sleep(3)
			os.system('clear')
			importlib.reload(importedmodules[dashdummy])
			alwaysonrd.ramslot3 = ""

		elif os.path.isdir(""+sysdir+"/"+speclaunch+"") == True and launch != "" and speclaunch in importedmodules:
			path = ""+sysdir+"/"+speclaunch+""
			os.chdir(""+path+"")
			sys.path.append(path)
			dummyapp = "init"+speclaunch
			importlib.reload(importedmodules[speclaunch])
			time.sleep(3)
			os.system('clear')
			importlib.reload(importedmodules[dashdummy])
			alwaysonrd.ramslot3 = ""
		else:
			print(""+launch+" is not a recognised python module or setup element.")
			time.sleep(2)
			alwaysonrd.ramslot3 = ""
			continue

	elif launch == "logout":
		print("Logging out.")
		alwaysonrd.usernamec = ""
		alwaysonrd.passwdc = ""
		break

	elif launch.startswith("/"):
		print(""+launch+" is not a recognised python module or setup element.")
		wasnothing = True
		alwaysonrd.ramslot3 = ""

	else:
		print(""+launch+" is not a recognised python module or setup element.")
		wasnothing = True
		alwaysonrd.ramslot3 = ""
