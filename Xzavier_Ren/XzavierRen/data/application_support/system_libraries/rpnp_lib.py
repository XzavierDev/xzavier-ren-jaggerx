def p1v0(code):
    series100 = ["100"]
    series200 = []
    series300 = ["300","301","302","303","304"]
    series400 = []
    series500 = []
    series600 = []
    series700 = []
    series800 = []
    series900 = ["900","901"]
    series1000 = ["1000","1001","1002"] 

    resp100 = ["Connection Accepted, All Requirements Satisfied for Preliminary"]
    resp200 = []
    resp300 = ["A Specified HEAD (header) aspect [aspect] is not supported. Non Lethal.",\
"A Specified HEAD (Header) aspect [aspect] is not supported. Lethal (901).",\
"Mode Specified in HEAD (Header) [mode] is not supported by this server. Lethal (901).",\
"Protocol version of connected machine is depreciated. Non Lethal.",\
"Protocol version of connected machine is obsolete. Lethal (901)."]
    resp400 = []
    resp500 = []
    resp600 = []
    resp700 = []
    resp800 = []
    resp900 = ["Connection Refused, Generic HEAD (header) requirements not met. Lethal (901).",\
"As Connection Was Refused, Closing Socket or Equivalent."]
    resp1000 = ["Connection Refused. Client Edition is Not Proprietary. Lethal (1001).",\
"As Connection Was Refused, Closing Socket or Equivalent.",\
"Connection Refused, Generic HEAD (header) requirements not met. Lethal (901)."]

    if code.startswith("1"):
        stringlist = series100
        messagelist = resp100
    elif code.startswith("2"):
        stringlist = series200
        messagelist = resp200
    elif code.startswith("3"):
        stringlist = series300
        messagelist = resp300
    elif code.startswith("9"):
        stringlist = series900
        messagelist = resp900
    elif code.startswith("1000"):
        stringlist = series1000
        messagelist = resp1000
    else:
        print ("Error.")

    count = 0
    message = "null"
    for number in stringlist:
        if number == code:
            message = messagelist[count]
            break
        else:
            count = count+1
            continue
    retval = code+": "+message
    return retval
            

def genhead(proto,mode,indicator,custom):
    include = False
    headpart = "<HEAD: proto= "+proto+", mode= "+mode+", indicator= "+indicator+""
    if custom != "":
        include = True
    else:
        include = False
    
    if include == True:
        headpart = headpart+", "+custom+":/HEAD>"
        head = headpart
    else:
        headpart = headpart+":/HEAD>"
        head = headpart
    return head


