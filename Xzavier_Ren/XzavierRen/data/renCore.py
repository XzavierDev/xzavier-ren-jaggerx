#! /usr/bin/env python3
# -*- coding: utf-8 -*-
AppName = "XZAVIERKERNEL"
MinKern = [0,9,0,0]
MinPyth = [3,5,0]
AppVersion = [0,9,0,0]
AppType = "CORE"
Framework1 = False
PlatformID = ["ALL"]
BranchID = "~all"
EditionID = ["_foundation"]
InstallType = "OAM"
DevID = 0000
#AppCode

import os
import time
import sys
import getpass
import linecache
import platform

if platform.system() == "Windows":
    home = os.environ['HOMEPATH']
else:
    home = os.environ['HOME']


bootToModLoader = linecache.getline(""+home+""+os.sep+"Documents"+os.sep+"recoveryinfo.cfg", 1)
if "4" in bootToModLoader and platform.system() != "Windows":
    import modLoaderModule
else:
    pass

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')

sys.path.append(""+workingdir+"/ramdisk")
sys.path.append(""+workingdir+"/users")
sys.path.append(""+workingdir+"/device_policy")
sys.path.append(""+workingdir+"/addons")
sys.path.append(""+workingdir+"/application_support/system_libraries")

import ramPrimerModule
import transientStorageModule as alwaysonrd

newshell = True

username = (alwaysonrd.usernamec)
password = (alwaysonrd.passwdc)

print ("Welcome, "+username+".")

#passwd = getpass.getpass("Please enter your password.> ")

#while True:
    #if passwd == password:
        #alwaysonrd.shellbit = True
        #os.system('clear')
        #import shell
    #else:
        #alwaysonrd.shellbit = False
        #passwd = getpass.getpass("Please try again, or press CTRL C to quit.> ")



if newshell == True:
    if os.path.exists(""+workingdir+""+os.sep+"winResetCatch.cfg"):
        import configurationModule
    else:
	    import shellModule
else:
	try:
		linecache.getline(""+workingdir+"/users/default/usrinfo.cfg", 1)
		import shellModule
	except FileNotFoundError:
		import setupModule
