#! /usr/bin/env python3
# -*- coding: utf-8 -*-
AppName = "SETUPWIZARD"
MinKern = [0,9,2,1]
MinPyth = [3,5,0]
AppVersion = [0,9,1,0]
AppType = "CORE"
Framework1 = False
PlatformID = ["ALL"]
BranchID = "~all"
EditionID = ["_foundation"]
InstallType = "OAM"
DevID = 0000
#AppCode

import os
ossep = os.sep

import linecache

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')
isnt = linecache.getline("dirinfo3.cfg", 2).rstrip('\n')

import sys
sys.path.append(""+workingdir+""+ossep+"ramdisk")
sys.path.append(""+workingdir+""+ossep+"application_support"+os.sep+"system_libraries")
import transientStorageModule as alwaysonrd
import getpass
import time
import osdescriptor as od
import platform

clear = ""

if platform.system() == "Windows":
	clear = "cls"
else:
	clear = "clear"

alwaysonrd.hostnamec = input("Enter a network hostname> ")
alwaysonrd.usernamec = input("Enter your username> ")
alwaysonrd.passwdc = ""
alwaysonrd.keyc = "NO-KEY"

devicetype = "standard"

deviceprofile = input("Does the device have a screen less than 10' or run Windows 8 or earlier?(Y/N)> ")
if deviceprofile == "y" or deviceprofile == "Y":
	devicetype = "mobile"
else:
	devicetype = "standard"

com = 6
alias = ""
while (int(com)) < 1 or int(com) > 5:
	print("""Specify your command to launch python,
if you're using windows in the default python3 configurations, you don't need a launch command.

1. python3 <scriptname>
2. python <scriptname>
3. py
4. <scriptname>
5. Custom
6. I don't need one.
\n""")

	com = input("OPTION(1-5)> ")
	if com == "1":
		alias == "python3"
	elif com == "2":
		alias = "python"
	elif com == "3":
		alias = "py"
	elif com == "4":
		alias = "NO-ALIAS"
	elif com == "5":
		alias = input("Enter your custom alias> ")
	elif com == "6":
		alias = "NO-ALIAS"

	else:
		pass
if com == "":
	alias = "NO-ALIAS"
else:
	pass




branch = input ("What Branch do you want to use? Development (d), Alpha (a), Master (m), Release (r), RailWing (rail)?> ")
print("""\nDo you want to enable telemetry to help us improve our services? This will only collect data on
how you use our application, and nothing else. Data sent includes app reliability statistics,
the version number of Xzavier Ren, platform and edition type that you are running;
as well as module versions and customization analytics.
Enabling telemetry also allows you to submit feedback.
We will NEVER collect personal information such as passwords or user created files.
For more information visit https://www.xzavierprojects.uk/""")
print("\n")
telemetry = input("Enable telemetry? Type YES to enable, or anything else to disable.> ")
if telemetry == "YES":
	print ("Telemetry has been enabled. Thanks for your support.")
	telemetrybit = True
else:
	print("Telemetry was not enabled.")
	telemetrybit = False

if telemetrybit == True:
	writeval = "1"
else:
	writeval = "0"

propbit = 1
os.system(""+clear+"")
print("""Xzavier Ren contains some proprietary components by default. These components are required to
support certain features like GameWork. These proprietary components are stored in the 'DRM' directory.
You have the option to disable these components, but do note that your experience will be sub-par
as a result. Please specify if you want to disable these components.\n""")

prop = input("Use Proprietary Components? Type 'NO' to refuse.> ")
if "NO" in prop:
	propbit = 0
else:
	propbit = 1


print ("Thanks for the info.. Configuring...")
os.system(""+clear+"")

updateserver = "github.com/Xzavier-Studio/Xzavier-Ren-Core/"

import os

os.chdir(workingdir)

print ("SAVING TO DISK...")

if branch == "d":
	branchid = "development"
elif branch == "a":
	branchid = "alpha"
elif branch == "m":
	branchid = "master (edge - release)"
elif branch == "r":
	branchid = "release"
elif branch == "rail":
	branchid = "railwing"
else:
	branchid = "BAD-BRANCH-SUPPLIED"

#Save Hostname and ID
settings = open("settings.cfg", "w")
settings.write(""+alwaysonrd.hostnamec+"\n"+alwaysonrd.keyc+"\n"+updateserver+"\n"+branchid+"\n"+devicetype+"\n"+alias+"\n"+(str(propbit))+"\n")
settings.close()


os.system(""+od.rm+" "+workingdir+""+os.sep+"users")
os.system("mkdir "+workingdir+""+os.sep+"users")

print("Creating user home area...")
if os.path.isdir(""+workingdir+""+os.sep+"users"+os.sep+""+alwaysonrd.usernamec+"") == True:
	pass
else:
	os.system("mkdir "+workingdir+""+os.sep+"users"+os.sep+""+alwaysonrd.usernamec+"")

os.chdir(""+workingdir+""+os.sep+"users"+os.sep+""+alwaysonrd.usernamec+"")
createuserconfigfile = open(""+workingdir+""+os.sep+"users"+os.sep+""+alwaysonrd.usernamec+""+os.sep+"usrinfo.cfg", "w")
createuserconfigfile.write(""+alwaysonrd.usernamec+"\nNOPASSWORD\nroot")
createuserconfigfile.close()

if isnt == '1':
	home = os.environ['HOMEPATH']
else:
	home = os.environ['HOME']

if isnt == "1":
	f = open("C:"+home+""+os.sep+"recoveryinfo.cfg","w")
else:
	f = open(""+home+""+os.sep+"recoveryinfo.cfg","w")

f.write("0\n")
f.close()


print ("DATA SAVED")

os.chdir(workingdir)

createsetupcheckfile = open("setupcheck.cfg", "w")
createsetupcheckfile.write("1\n"+writeval+"")

import time
time.sleep(3)
pass

print ("Setup is complete, press ENTER key to exit")
exitcall = input("")
exit()
