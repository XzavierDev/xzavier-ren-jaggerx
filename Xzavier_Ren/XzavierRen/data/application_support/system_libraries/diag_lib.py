#! /usr/bin/env python3
# -*- coding: utf-8 -*-

#This diagnostics module logs, collects and sends telemetry.

#imports
import os
import time
import sys
import linecache
import socket
import datetime
import platform

import rpnp_lib
import os_lib
import transientStorageModule as ts

#workingdir

def sendToServer(data):
    print("Not implemented.")

def sendToThirdParty(party,application,data):
    print("Not implemented.")

def downloadExternalDiagTool(tool,source):
    print("Not Implemented.")

def genSystemProfile(workingdir):
    #jaggerx
    return False

    build = linecache.getline("buildprop.xcf", 7).rstrip("\n")
    displayVersion = linecache.getline("buildprop.xcf", 3).rstrip("\n")
    strongVersionString = linecache.getline("buildprop.xcf", 9).rstrip("\n")
    edition = linecache.getline("buildprop.xcf", 1).rstrip("\n")
    uin = linecache.getline(""+workingdir+"/dynamic_content/global/installationConfig.cfg", 1).rstrip("\n")

    platformversion = platform.version()
    platsep = ":"
    shortened = platformversion.split(platsep, 1)[0]

    operatingSystem = platform.system()
    operatingSystemVersion = shortened

    uts = time.time()
    suts = (str(uts))
    ts = datetime.datetime.fromtimestamp(uts).strftime('%Y-%m-%d %H:%M:%S')

    if os.path.exists(""+workingdir+""+os.sep+"logs"):
        pass
    else:
        os.system ("mkdir "+workingdir+""+os.sep+"logs")

    appList = []
    for apps in os.listdir(""+workingdir+""+os.sep+"apps"+os.sep+"apps"):
        appList.append(apps)
    rAppList = (str(appList))
    f = open(""+workingdir+""+os.sep+"logs"+os.sep+"sysprofile_"+suts+".omricsysp", "w")
    f.write("OMRIC SYSTEM PROFILE:\nBUILD: "+build+"\nDISP.VER: "+displayVersion+"\nS.VER: "+strongVersionString+"\nEDITION: "+edition+"\nOS: "+operatingSystem+"\nOS_VER: "+operatingSystemVersion+"\nINST.APPS: "+rAppList+"")
    f.close()

def genLog(application,data,startnew):
    pass
def clearTransientLog():
    print("Not implemented.")

def clearAllLogs():
    pass

def genLogPackage(logfile,timestamp,sysprofile):
    pass

def crashScreen(application,userDisplayInformation,loginfo):
    #jaggerx
    return False
    print(""":( Something went wrong.\n
Ren needs to restart.\n

If this keeps happening consider performing a reset on your installation.
If this happens when you run an app, consult the developer
for more information or stop using the app.\n

If you have telemetry enabled, this crash may be submitted as
part of diagnostics.\n

Troubeshooting Info: """+userDisplayInformation+"""
""")
    data = ""
    startnew = True
    if loginfo != "":
        data = loginfo
        genLog(application,data,startnew)
