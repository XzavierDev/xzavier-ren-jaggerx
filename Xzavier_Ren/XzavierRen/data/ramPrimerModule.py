#! /usr/bin/env python3
# -*- coding: utf-8 -*-
AppName = "ramprimer"
MinKern = [0,9,0,0]
MinPyth = [3,5,0]
AppVersion = [1,0,0]
AppType = "CORE"
Framework1 = False
PlatformID = ["ALL"]
EditionID = ["_foundation"]
InstallType = "OAM"
DevID = 0000
#AppCode

import linecache

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')

import sys
import os
sys.path.append(""+workingdir+"/ramdisk")
import transientStorageModule as alwaysonrd

os.chdir(workingdir)

if os.path.isfile(""+workingdir+"/dynamic_content/global/systemPreferencesList.cfg") == True:
	alwaysonrd.hostnamec = linecache.getline("settings.cfg", 1).rstrip("\n")
	alwaysonrd.keyc = linecache.getline("settings.cfg", 2).rstrip("\n")

	import loginModule

	#os.chdir(""+workingdir+"/users/default")

	#username = linecache.getline("usrinfo.xif", 1).rstrip("\n")
	#password = linecache.getline("usrinfo.xif", 2).rstrip("\n")

	#alwaysonrd.usernamec = (username)
	#alwaysonrd.passwdc = (password)
else:
	alwaysonrd.usernamec = ""
	alwaysonrd.passwdc = ""

os.chdir(workingdir)

alwaysonrd.nvramslot1 = linecache.getline("nvramdump.cfg", 1).rstrip("\n")
alwaysonrd.nvramslot2 = linecache.getline("nvramdump.cfg", 2).rstrip("\n")
alwaysonrd.nvramslot3 = linecache.getline("nvramdump.cfg", 3).rstrip("\n")
alwaysonrd.nvramslot4 = linecache.getline("nvramdump.cfg", 4).rstrip("\n")
