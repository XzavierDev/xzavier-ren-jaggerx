def advancedconfig():
    print("Not Implemented.")

def manualoptions():
    print("""Choose an option:
1. Change the domain this device is attached to (unavailable).
2. Configure Arm and Hammer anti-malware protection (unavailable).
3. Change the screen profile (unavailable)
4. Change Edition (unavailable)
""")
    opt = input("1-4-Q> ")

while True:
    print("""Welcome to the Advanced Configuration Utility for Xzavier Ren.
Please choose an option:
1. Run the advanced configuration setup wizard.
2. Adjust some options manually.
3. Quit
""")
    com = input("OPTION(1-3-Q)> ")

    if com == "1":
        advancedconfig()
    elif com == "2":
        manualoptions()
    elif com == "Q" or com == "q" or com == "3":
        break
    else:
        continue