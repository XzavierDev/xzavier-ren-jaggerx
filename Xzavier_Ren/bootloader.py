import os
import os
import time
import linecache
import sys
import platform
isnt = "0"
workingdir = linecache.getline("dirinfo.cfg", 1).rstrip("\n")
targetdir = ""+workingdir+"/XzavierRen"
if "Windows" in platform.system():
	isnt = "1"
else:
	isnt = "0"
if isnt == "0":
	os.system('clear')
else:
	os.system('cls')
	targetdir = ""+workingdir+"\\XzavierRen"






notinlist = [""]
donotprint3 = False
donotprint4 = False
bootref = ""

bootopt1 = linecache.getline('bootlist.xcf', 1).rstrip('\n')
bootopt2 = linecache.getline('bootlist.xcf', 2).rstrip('\n')
bootopt3 = linecache.getline('bootlist.xcf', 3).rstrip('\n')
bootopt4 = linecache.getline('bootlist.xcf', 4).rstrip('\n')

if bootopt3 =="":
	notinlist.append(bootopt3)
	notinlist.append(bootopt4)
elif bootopt4 =="":
	notinlist.append(bootopt4)
else:
	pass


non_standard_is_disabled = False
quickboot_is_enabled = True

boottorecovery = False
if isnt != "1":
	home = os.environ['HOME']
	recoverybit = linecache.getline(""+home+"/Documents/recoveryinfo.cfg", 1).rstrip('\n')
else:
	home = os.environ['HOMEPATH']
	home = "C:"+(home)+"\\Documents"
	recoverybit = linecache.getline(""+home+"\\recoveryinfo.cfg", 1).rstrip('\n')
if "1" in recoverybit:
	boottorecovery = True
else:
	pass
if boottorecovery == True:
	print("BOOTING TO RECOVERY.")
	os.chdir(""+targetdir+"/-recovery")
	sys.path.append(""+targetdir+"/-recovery")
	import recovery
	exit()
else:
	pass

if quickboot_is_enabled == True:
	bootref = ""+bootopt1+""
	time.sleep(1)
else:
	print(""""
	Please Choose Your Desired Operating Environment:

	1 = """+bootopt1+"""
	2 = """+bootopt2+"""
	""")

	if bootopt3 in notinlist:
		donotprint3 = True
	elif bootopt4 in notinlist:
		donotprint4 = True
	else:
		pass

	if non_standard_is_disabled == True:
		donotprint3 = True
		donotprint4 = True
	else:
		pass

	if donotprint3 == False and donotprint4 == False:
		print (
"""	3 = """+bootopt3+"""
	4 = """+bootopt4+"""
		""")
	elif donotprint3 == False and donotprint4 == True:
		print("""
		3 = """+bootopt3+"""
		""")
	else:
		pass

	bootto = input("Boot To Number> ")
	if bootto == "1":
		bootref = ""+bootopt1+""
	elif bootto == "2":
		bootref = ""+bootopt2+""
	elif bootto == "3":
		bootref = ""+bootopt3+""
	elif bootto == "4":
		bootref = ""+bootopt4+""
	else:
		os.system('exit')


import sys

writeoutdir = open(""+targetdir+"/dirinfo2.cfg","w")
writeoutdir.write(""+targetdir+"\n"+isnt+"\n")
writeoutdir.close()

os.chdir(targetdir)
sys.path.append(targetdir)
import chainlink
