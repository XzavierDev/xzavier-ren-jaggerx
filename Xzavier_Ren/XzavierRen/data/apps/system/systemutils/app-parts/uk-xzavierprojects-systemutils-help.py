print("""systemutils - help
Ren System Utils:

This application configures your Ren installation. It requires an argument.
Arguments available are:

/usercfg - Configure user accounts, add delete and conduct other user centric operations.
/advcfg - Configure advanced config options, like joining a domain or changing safety settings.
/screentest - Check your terminal is the right size to display Xzavier Ren optimised content.

""")
con = input("Press ENTER to exit.")