#! /usr/bin/env python3
# -*- coding: utf-8 -*-
AppName = "Dash"
MinKern = [0,9,0,0]
MinPyth = [3,5,0]
AppVersion = [0,9,0,0]
AppType = "CORE"
Framework1 = False
PlatformID = ["ALL"]
EditionID = ["_foundation"]
InstallType = "OAM"
DevID = 0000
#AppCode

import os
import linecache
import sys

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')

version = ""


sys.path.append(""+workingdir+"/ramdisk")
import transientStorageModule as alwaysonrd


version = linecache.getline(""+workingdir+"/buildprop.xcf", 3)
print ("""
Xzavier Ren jaggerx v"""+version+"""
Copyright © 2016-2018 Rylan Kivolski.
Type 'help' for a list of commands
""")
