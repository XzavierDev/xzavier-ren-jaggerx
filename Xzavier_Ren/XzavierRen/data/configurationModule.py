#!"+os.sep+"usr"+os.sep+"bin"+os.sep+"env python3
#-*-coding: utf-8-*-
SYS_NAME = "configurationModule"
SYS_VERSION = "0925"
CRITICAL = True
MODPACK = "stock"
TESTED_ON = ["0925"]
COMPATIBLE_PLATFORMS = ["NIX"]
MIN_PY_VERSION = "360"
#End of mandatory variables
#imports
import sys
import os
import linecache
import time
import random

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')

sys.path.append(""+workingdir+""+os.sep+"ramdisk")
sys.path.append(""+workingdir+""+os.sep+"application_support"+os.sep+"system_libraries")
sys.path.append(""+workingdir+""+os.sep+"application_support"+os.sep+"system_apis")

import transientStorageModule as ts
import notifyAPI as na
import os_lib as ol
import stateManagementModule as smm
import diag_lib

def writeConfig(conf,seedstring,user="NONE"):
    workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')
    try:
        os.makedirs(""+workingdir+""+os.sep+"dynamic_content"+os.sep+"global"+os.sep+"")
    except FileExistsError:
        pass
    configurationFile = open(""+workingdir+""+os.sep+"dynamic_content"+os.sep+"global"+os.sep+"systemPreferencesList.cfg", "w")
    isAuto = False
    if conf == ["key:NO_KEY","devicetype:standard","launchcommand:python3","branch:preflight","updateserver:https://gitlab.com/XzavierDev/Xzavier-Ren-Core","alphaenabled:False","compatibilitycheckdisabled:True","debugmode:True","kioskmode:False","demomode:False"]:
        isAuto = True
    else:
        isAuto = False
    for item in conf:
        configurationFile.write(""+item+"\n")
    configurationFile.close()
    checkfile = open(""+workingdir+""+os.sep+"dynamic_content"+os.sep+"global"+os.sep+"systemPreferencesCheck.cfg", "w")
    checkfile.write("1")
    checkfile.close()
    if os.path.exists(""+workingdir+""+os.sep+"dynamic_content"+os.sep+"global"+os.sep+"installationConfig.cfg") == False:
        uinfile = open(""+workingdir+""+os.sep+"dynamic_content"+os.sep+"global"+os.sep+"installationConfig.cfg", "w")
        uinfile.write(seedstring)
        uinfile.close()
    else:
        pass

    while True:
        isAuto = True
        if isAuto == True:
            if os.path.exists(""+workingdir+""+os.sep+"users"+os.sep+"jagger"+os.sep+"") == True:
                userconfig = open(""+workingdir+""+os.sep+"users"+os.sep+"jagger"+os.sep+"userSettings.cfg","w")
                userconfig.write("jagger")
                userconfig.close()
                break
            else:
                os.system("mkdir "+workingdir+""+os.sep+"users"+os.sep+"")
                os.system("mkdir "+workingdir+""+os.sep+"users"+os.sep+"jagger"+os.sep+"")
                userconfig = open(""+workingdir+""+os.sep+"users"+os.sep+"jagger"+os.sep+"userSettings.cfg","w")
                userconfig.write("jagger\nNOPASSWORD")
                userconfig.close()
                break
        else:
            skip = False
            try:
                if user == "NONE":
                    skip = True
                else:
                    skip = False
                if skip == True:
                    os.system("mkdir "+workingdir+""+os.sep+"users"+os.sep+"")
                    os.system("mkdir "+workingdir+""+os.sep+"users"+os.sep+""+user+""+os.sep+"")
                    userconfig = open(""+workingdir+""+os.sep+"users"+os.sep+""+user+""+os.sep+"userSettings.cfg","w")
                    userconfig.write(""+user+"")
                    userconfig.close()
                else:
                    break
            except OSError:
                break

    diag_lib.genSystemProfile(workingdir)
    return True



def setup():
    print("Checking system configuration...")
    autoconfig = True
    if autoconfig == True:
        user = "jagger"
        defaultconfig = ["key:NO_KEY","devicetype:jagger","launchcommand:python","branch:jaggerx","updateserver:https://gitlab.com/XzavierDev/Xzavier-Ren-jaggerx","alphaenabled:False","compatibilitycheckdisabled:True","debugmode:False","kioskmode:False","demomode:False"]
        print("Generating installation seed...")
        seedlist = []
        for x in range(10):
        	seed = random.randint(1,50)
        	seed = (str(seed))
        	seedlist.append(seed)
        seedstring = (str(seedlist))
        for char in seedstring:
        	charlist = ("[","]",",","'"," ")
        	if char in charlist:
        		seedstring = seedstring.replace(""+char+"", "")
        	else:
        		pass
        print("Seed generated.")
        if writeConfig(defaultconfig,seedstring,user) == True:
            print("The default configuration has been applied (username: jagger)")
            cont = input("ENTER to exit...")
            smm.terminate()
        else:
            exit()
    else:
        print("This version introduces a new preference system. Please complete setup again.")
        print("You can keep your old user by pressing enter"+os.sep+"return at the username prompt.")
        config = ["key:NO_KEY","devicetype:standard","updateserver:https:"+os.sep+""+os.sep+"gitlab.com"+os.sep+"XzavierDev"+os.sep+"Xzavier-Ren-Core","alphaenabled:False","compatibilitycheckdisabled:False","debugmode:False","kioskmode:False","demomode:False"]

        while True:
            user = input("Please enter a new username (Optional)> ")
            validusers = []
            for user in os.path.isdir(""+workingdir+""+os.sep+"users"+os.sep+""):
                validusers.append(user)
                if (len(validusers)) <= 1:
                    print("You cannot skip this step as no users exist.")
                    continue
                else:
                    print("You have selected to continue using your old account.")
                    break
        launchcom = input("Please enter the command you use to launch Python3 from the terminal> ")
        prefix = "launchcommand:"
        add = prefix+launchcom
        config.append(add)
        branch = "release"

        print("Generating installation seed...")
        seedlist = []
        for x in range(10):
        	seed = random.randint(1,50)
        	print (seed)
        	seed = (str(seed))
        	seedlist.append(seed)
        seedstring = (str(seedlist))
        for char in seedstring:
        	charlist = ("[","]",",","'"," ")
        	if char in charlist:
        		seedstring = seedstring.replace(""+char+"", "")
        	else:
        		pass
        print("Seed generated.")

        print("Saving configuration...")
        if writeConfig(config,seedstring,user) == True:
            print("Setup complete.")
            cont = input("PRESS ENTER TO EXIT.")
            smm.terminate()
        else:
            print("ERROR.")
            cont = input("PRESS ENTER.")
            exit()
