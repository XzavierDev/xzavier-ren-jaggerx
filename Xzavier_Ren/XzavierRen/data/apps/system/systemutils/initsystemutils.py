PlatformID = ["ALL"]
BranchID = "~all"
EditionID = ["_foundation"]
InstallType = "OAM"
#CUSTOMISED_MARKER_NORMAL
#AppCode
import transientStorageModule as alwaysonrd
import os
import sys
import importlib
import linecache
import time

workingdir = linecache.getline("dirinfo4.cfg", 1).rstrip('\n')
changeto = (""+workingdir+""+os.sep+"apps"+os.sep+"system"+os.sep+"systemutils"+os.sep+"app-parts")

did = 'uk-xzavierprojects-systemutils-'
dummy = (''+did+''+alwaysonrd.ramslot3+'')


importedmodules = {''+did+'usercfg':''+did+'usercfg',''+did+'help':''+did+'help',''+did+'advcfg':''+did+'advcfg'}


module = ""+alwaysonrd.ramslot3+""

if alwaysonrd.ramslot3 == "":
    switch = False
    print("systemutils v0.9.2.1 - Xzavier Ren. This application requires you to submit an argument.")
    time.sleep(2)

else:
    try:
        os.chdir(""+workingdir+""+os.sep+"apps"+os.sep+"system"+os.sep+"systemutils"+os.sep+"app-parts")
        sys.path.append(""+workingdir+""+os.sep+"apps"+os.sep+"system"+os.sep+"systemutils"+os.sep+"app-parts")
        'if alwaysonrd.bool1 == False:'
        if dummy not in vars():
            vars()[dummy] = importlib.import_module(dummy)
            alwaysonrd.bool1 = True
        else:
            importlib.reload(vars()[dummy])
    except ImportError:
        print("The argument is invalid (app)")
