#! /usr/bin/env python3
# -*- coding: utf-8 -*-
AppName = "System Info "
MinKern = [0,9,0,0]
MinPyth = [3,5,0]
AppVersion = [0,9,0,0]
AppType = "CORE-SAD"
Framework1 = False
PlatformID = ["ALL"]
EditionID = ["_foundation"]
InstallType = "OAM"
DevID = 0000
#AppCode

import os
import linecache
import sys
import platform
import transientStorageModule as alwaysonrd
import importlib
import time

did = "uk-xzavierprojects-sysinfo-"
importedmodules = {''+did+'help':''+did+'help'}
module = ""+alwaysonrd.ramslot3+""
dummy = (""+did+""+alwaysonrd.ramslot3+"")
workingdir = linecache.getline("dirinfo4.cfg", 1).rstrip('\n')
if alwaysonrd.ramslot3 == "":

  sys.path.append(""+workingdir+"/ramdisk")
  sys.path.append(""+workingdir+"/device_policy")
  import osdescriptor as od



  uiid = linecache.getline(""+workingdir+"/settings.cfg", 2).rstrip("\n")
  user = alwaysonrd.usernamec
  editionid = linecache.getline(""+workingdir+"/buildprop.xcf", 1).rstrip("\n")
  updatelevel = linecache.getline(""+workingdir+"/buildprop.xcf", 3).rstrip("\n")
  platformid = platform.system()
  ntno = linecache.getline(""+workingdir+"/buildprop.xcf", 5).rstrip("\n")
  branch = linecache.getline(""+workingdir+"/settings.cfg", 4).rstrip("\n")
  platformversion = platform.version()
  platsep = ":"
  shortened = platformversion.split(platsep, 1)[0]
  pythonversion = platform.python_version()
  buildno = linecache.getline(""+workingdir+"/buildprop.xcf", 6).rstrip("\n")
  buildinf = linecache.getline(""+workingdir+"/buildprop.xcf", 7).rstrip("\n")


  if editionid == "_foundation":
	  edition = "Ren Core"
	  editiontype = "Desktop"
  elif editionid == "_home":
    edition = "Ren Home"
  elif editionid == "_professional":
	  edition = "Ren Plus"
  elif editionid == "_enterprise":
	  edition = "Ren Enterprise"
  elif editionid == "_enterprise+":
	  edition = "Ren Enterprise Plus"
  else:
	  editionid = "SUPPORT"
	  edition = "PLEASE"
	  editiontype = "Compatibility layer"

  if od.isdebug == True:
    opt = "THIS IS AN ENGINEERING BUILD. DEVELOPMENT USE ONLY."
  else:
    opt = ""



  os.system(''+od.cl+'')
  print   ("""
                 Xzavier Ren System Information Utility.
                    Copyright © 2013-2018 Rylan Kivolski.
                  Free to Use, Modify and Distribute.
		 """)
  print    ("""
                      Host Name: """+alwaysonrd.hostnamec+"""
                      Edition Type: """+editiontype+"""
                      Build Number: """+buildinf+"""
                      Revision: """+buildno+"""
                      Codename: Shenx
                      Python Version: """+pythonversion+"""
                      Compatibility Version: """+updatelevel+"""
                      Platform Kernel Type: """+platformid+"""
                      Platform Kernel Version: """+shortened+"""
                      Progression Level: N/A
                      Version Key: This build has not passed Q.A.\n
                      """+opt+"""
												""")

  com = input("Press ENTER to quit.")

else:
  try:
        os.chdir(""+workingdir+""+os.sep+"apps"+os.sep+"system"+os.sep+"sysinfo"+os.sep+"app-parts")
        sys.path.append(""+workingdir+""+os.sep+"apps"+os.sep+"system"+os.sep+"sysinfo"+os.sep+"app-parts")
        'if alwaysonrd.bool1 == False:'
        if dummy not in vars():
            vars()[dummy] = importlib.import_module(dummy)
            alwaysonrd.bool1 = True
        else:
            importlib.reload(vars()[dummy])
  except ImportError:
        print("The argument is invalid")
