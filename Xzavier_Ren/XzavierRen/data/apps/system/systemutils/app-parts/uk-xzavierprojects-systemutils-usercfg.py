import linecache
import os
import time
import sys

workingdir = linecache.getline("dirinfo4.cfg", 1).rstrip("\n")

sys.path.append(""+workingdir+""+os.sep+"device_policy")
import osdescriptor as od

autologin = False



print("Use this module to configure users...")
print("""Select an option from the menu below:
1. Add a new user account.
2. Delete an existing user account.
3. Change permissions on a user account. (unavailable)
4. Configure auto-login. (non working)
5. Change the password for a user account. (unavailable)
q. Quit
\n""")

while True:
	com = input("OPTION(1-5)> ")
	if com == "1":
		usradd = input("Type the name of the new user> ")
		usrpass = input("Type a password. Leave blank for none> ")
		if usrpass == "":
			usrpass = "NOPASSWORD"
		else:
			pass
		print("Creating user home area...")
		if os.path.isdir(""+workingdir+""+os.sep+"users"+usradd+""):
			print("ERROR: Username already taken.")
			time.sleep(2)
			continue
		else:
			os.system("mkdir "+workingdir+""+os.sep+"users"+os.sep+""+usradd+"")
			print ("Home area created!")
			print ("Adding user info...")
			profile = open(""+workingdir+""+os.sep+"users"+os.sep+""+usradd+""+os.sep+"usrinfo.cfg", "w")
			profile.write(""+usradd+"\n"+usrpass+"\nroot")
			profile.close()
			print("User Created!")
			
	elif com == "2":
		com = input("Type the name of the user you want to delete> ")
		try:
			os.chdir(""+workingdir+""+os.sep+"users")
			os.system(""+od.rm+" "+workingdir+""+os.sep+"users"+os.sep+""+com+"")
			print ("USER DELETED.")
			continue
		except:
			print ("FAILED. Invalid user/path.")
			continue
			
	elif com == "3":
		print ("This feature is not implemented.")
		
	elif com == "4":
		print("Unavailable")
		continue
		com = input("Type the name of the user you want to auto login> ")
		passwd = input("Type the password for "+com+"> ")
		try:
			storedpass = linecache.getline(""+workingdir+""+os.sep+"users"+os.sep+""+com+""+os.sep+"usrinfo.cfg", 2)
			if storedpass == "NOPASSWORD":
				check = ""
			else:
				check = storedpass.rstrip("\n")
			if passwd == check:
				autologin = True
				read = open (""+workingdir+""+os.sep+"users"+os.sep+""+com+""+os.sep+"usrinfo.cfg", "r")
				readstore = read.readlines()
				read.close()
				readstore[1] = "NOPASSWORD"
				write = open (""+workingdir+""+os.sep+"users"+os.sep+""+com+""+os.sep+"usrinfo.cfg", "w")
				for item in readstore:
					write.write("\n".join(readstore))
				write.close()
				print("Autologin is now enabled.")
					
			else:
				print ("Password Invalid.")
				continue
		except:
			print ("User does not exist.")
			
	elif com == "5":
		("This feature is not implemented.")
		
	elif com == "q" or com == "Q":
		print ("Exiting...")
		break
		
	else:
		print ("Bad command.")
	
			
			
		
		
	
