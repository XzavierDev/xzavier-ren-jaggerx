import platform
import linecache
import os

isdebug = True
renversion = [0,9,2,5]
renVersionString = "925"
operatingSystem = "Unknown"

systemindicator = "NULL"
unix = True
if platform.system() == "Windows":
	unix = False
	systemindicator = "Windows NT"
else:
	unix = True
	systemindicator = "UNIX derivative"

if unix == True:
	cl = "clear"
	rm = "rm -rf"
	rmf = "rm -rf"
	cp = "cp"
	cpr = "cp -r"
	home = os.environ['HOME']+os.sep+"Documents"
else:
	cl = "cls"
	rm = "rmdir /s /q "
	rmf = "del"
	cp = "copy"
	cpr = "copy"
	homePath = os.environ['HOMEPATH']
	home = "C:"+(homePath)+os.sep+"Documents"

if linecache.getline("systemindicator.cfg", 1) == "Mobile\n":
	mobile = True
else:
	mobile = False
