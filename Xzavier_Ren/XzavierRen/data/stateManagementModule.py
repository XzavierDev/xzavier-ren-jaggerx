#!/usr/bin/env python3
#-*-coding: utf-8-*-
SYS_NAME = "stateManagementModule"
SYS_VERSION = "0925"
CRITICAL = True
MODPACK = "stock"
TESTED_ON = ["0925"]
COMPATIBLE_PLATFORMS = ["NIX"]
MIN_PY_VERSION = "360"
#End of mandatory variables

import os
import linecache
import sys
import diag_lib
import transientStorageModule as ts
import os_lib

workingdir = linecache.getline("dirinfo3.cfg", 1).rstrip('\n')

def initialise():
    #jaggerx
    print("State management is not supported is jaggerx, ignoring.")
    return True
    f = open(""+workingdir+""+os.sep+"state.cfg", "w")
    try:
        ts.nvramslot1 = linecache.getline(""+workingdir+""+os.sep+"nvramdump.cfg", 1).rstrip("\n")
        ts.nvramslot2 = linecache.getline(""+workingdir+""+os.sep+"nvramdump.cfg", 2).rstrip("\n")
        ts.nvramslot3 = linecache.getline(""+workingdir+""+os.sep+"nvramdump.cfg", 3).rstrip("\n")
        ts.nvramslot4 = linecache.getline(""+workingdir+""+os.sep+"nvramdump.cfg", 4).rstrip("\n")
        os.system(""+os_lib.rmf+" "+workingdir+""+os.sep+"nvramdump.cfg")
    except FileNotFoundError:
        pass
    f.write("INITIALISED")
    f.close()

def terminate():
    #jaggerx
    print("State management is not supported is jaggerx, ignoring.")
    return True
    d = open(""+workingdir+""+os.sep+"nvramdump.cfg", "w")
    d.write(""+ts.nvramslot1+"\n"+ts.nvramslot2+"\n"+ts.nvramslot3+"\n"+ts.nvramslot4+"\n")
    d.close()
    f = open(""+workingdir+""+os.sep+"state.cfg", "w")
    f.write("TERMINATED")
    f.close()
    exit()

def unexpectedTerminationHandler(reason):
    #jaggerx
    print("State management is not supported is jaggerx, ignoring.")
    return True
    f = open("state.cfg", "w")
    f.write("TERMINATED")
    f.close()

    print(""":( ATTENTION!
    This instance was not correctly terminated because you
    did not correctly shut down the application or an app
    you tried to run crashed.

    To terminate the application, type 'terminate' at the shell
    prompt. This is important, as it allows the environment to
    save data before it shuts down.

    Press ENTER to continue.""")
    cont = input("")

def appCrashSystem(reason):
    #jaggerx
    print("State management is not supported is jaggerx, ignoring.")
    return True
    print(""":( Something went wrong.

	Xzavier Ren has encountered a serious error and needs to terminate.
	This may be because you are not running an up to date version
	of the O.E, you modified critical application files
	or there is a bug in the application. Please try to
    update your software before continuing.\n""")
    print("REASON: "+reason+"")
    print("Press ENTER to exit.")
    cont = input ("")
    exit()

def appCrash(app,reason):
    #jaggerx
    print("State management is not supported is jaggerx, ignoring.")
    return True
    os.system(""+os_lib.cl+"")
    print(""":( Something went wrong.

    """+app+""" has crashed. Please check for an update to the
    application before continuing. If this continues, try reinstalling
    the app.

    REASON: """+reason+""". """)
    cont = input("PRESS ENTER TO CONTINUE")
